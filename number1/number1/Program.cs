﻿using System;

namespace number1
{
    class Program
    {
        static void Main(string[] args)
        {

            double y;


            double x = Convert.ToDouble(Console.ReadLine());
            if (x >= 20)
            {
                y = 2 * x - 2;
                Console.WriteLine(y);
            }
            else if (0 <= x && x < 20)
            {
                y = 5 + x;
                Console.WriteLine(y);
            }
            else if (x < 0)
            {
                y = 2 * Math.Abs(x);
                Console.WriteLine(y);
            }
            Console.ReadKey();




        }
    }
}
